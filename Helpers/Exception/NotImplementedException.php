<?php

namespace wzorce\Helpers\Exception;

use RuntimeException;

class NotImplementedException extends RuntimeException
{
    protected $message = 'To be implemented';
}