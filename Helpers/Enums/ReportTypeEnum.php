<?php

namespace wzorce\Helpers\Enums;

enum ReportTypeEnum: string
{
    case PDF = 'Pdf';
    case HTML = 'Html';
    case CSV = 'Csv';
    case TXT = 'Txt';
}
