<?php

namespace wzorce\strukturalne\fasada\Fasade;

use wzorce\strukturalne\fasada\Interface\ReportInterface;
use wzorce\strukturalne\fasada\Model\ReportData;
use wzorce\strukturalne\fasada\Model\ReportFormatter;
use wzorce\strukturalne\fasada\Model\ReportGenerator;

readonly class ReportFacade implements ReportInterface
{
    public function __construct(
        private ReportData $data,
        private ReportFormatter $formatter,
        private ReportGenerator $generator
    ) {
    }

    public function generate(): string
    {
        $data = $this->data->getData();
        $formattedData = $this->formatter->format($data);
        return $this->generator->generateReport($formattedData);
    }
}
