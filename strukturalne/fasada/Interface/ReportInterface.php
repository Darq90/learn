<?php

namespace wzorce\strukturalne\fasada\Interface;

interface ReportInterface
{
    public function generate(): string;
}
