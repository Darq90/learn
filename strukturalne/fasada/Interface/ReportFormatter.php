<?php

namespace wzorce\strukturalne\fasada\Interface;

interface ReportFormatter
{
    public function format(string $data): string;
}
