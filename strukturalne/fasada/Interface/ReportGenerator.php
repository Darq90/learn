<?php

namespace wzorce\strukturalne\fasada\Interface;

interface ReportGenerator
{
    public function generateReport(string $data): string;
}
