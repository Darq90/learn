<?php

namespace wzorce\strukturalne\fasada\Interface;

interface ReportDataInterface
{
    public function getData(): string;
}