<?php

namespace wzorce\strukturalne\fasada\Model;

class ReportFormatter implements \wzorce\strukturalne\fasada\Interface\ReportFormatter
{

    public function format(string $data): string
    {
        return "Formatted: {$data}";
    }
}
