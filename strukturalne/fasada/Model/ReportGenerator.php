<?php

namespace wzorce\strukturalne\fasada\Model;

class ReportGenerator implements \wzorce\strukturalne\fasada\Interface\ReportGenerator
{

    public function generateReport(string $data): string
    {
        return "Generated report: {$data}";
    }
}
