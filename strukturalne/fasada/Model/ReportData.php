<?php

namespace wzorce\strukturalne\fasada\Model;

use wzorce\strukturalne\fasada\Interface\ReportDataInterface;

class ReportData implements ReportDataInterface
{

    public function getData(): string
    {
        return "Report data";
    }
}
