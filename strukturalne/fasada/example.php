<?php

use wzorce\strukturalne\fasada\Fasade\ReportFacade;
use wzorce\strukturalne\fasada\Model\ReportData;
use wzorce\strukturalne\fasada\Model\ReportFormatter;
use wzorce\strukturalne\fasada\Model\ReportGenerator;

$facade = new ReportFacade(
    new ReportData(),
    new ReportFormatter(),
    new ReportGenerator()
);

echo $facade->generate(); // Generated report: Formatted: Report data
