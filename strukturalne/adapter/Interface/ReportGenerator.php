<?php

namespace wzorce\strukturalne\adapter\Interface;

interface ReportGenerator
{
    public function generate(string $data): void;
}
