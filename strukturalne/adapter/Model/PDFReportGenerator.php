<?php

namespace wzorce\strukturalne\adapter\Model;

use wzorce\strukturalne\adapter\Interface\ReportGenerator;

class PDFReportGenerator implements ReportGenerator
{
    public function generate(string $data): void
    {
        print "PDF report with data: {$data}";
    }
}
