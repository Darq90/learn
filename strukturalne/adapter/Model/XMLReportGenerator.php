<?php

namespace wzorce\strukturalne\adapter\Model;

class XMLReportGenerator
{
    public function generateXml(string $data): string
    {
        return "<xml>{$data}</xml>";
    }
}
