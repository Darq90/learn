<?php

use wzorce\strukturalne\adapter\Adapter\XMLReportAdapter;
use wzorce\strukturalne\adapter\Model\PDFReportGenerator;
use wzorce\strukturalne\adapter\Model\XMLReportGenerator;

$pdfReportGenerator = new PDFReportGenerator();
$xmlReportGenerator = new XMLReportGenerator();

$xmlAdapter = new XMLReportAdapter($xmlReportGenerator);

$data = "Some report data";

$pdfReportGenerator->generate($data); // PDF report with data: Some report data
$xmlAdapter->generate($data); // Converting XML report to PDF format: <xml>Some report data</xml>
