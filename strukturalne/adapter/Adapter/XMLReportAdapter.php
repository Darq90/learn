<?php

namespace wzorce\strukturalne\adapter\Adapter;

use wzorce\strukturalne\adapter\Interface\ReportGenerator;
use wzorce\strukturalne\adapter\Model\XMLReportGenerator;

readonly class XMLReportAdapter implements ReportGenerator
{
    public function __construct(
        private XMLReportGenerator $XMLReportGenerator
    ) {
    }

    public function generate(string $data): void
    {
        $xmlData = $this->XMLReportGenerator->generateXml($data);
        print "Converting XML report to PDF format: {$xmlData}";
    }
}
