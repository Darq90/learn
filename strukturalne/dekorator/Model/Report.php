<?php

namespace wzorce\strukturalne\dekorator\Model;

use wzorce\strukturalne\dekorator\Interface\ReportInterface;

class Report implements ReportInterface
{
    public function generate(): string
    {
        return "Generating report";
    }
}
