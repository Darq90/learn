<?php

use wzorce\strukturalne\dekorator\Decorator\DescriptionDekorator;
use wzorce\strukturalne\dekorator\Decorator\JsonDecorator;
use wzorce\strukturalne\dekorator\Model\Report;

$report = new Report();
$descriptionDecorator = new DescriptionDekorator($report);
$jsonDecorator = new JsonDecorator($report);

echo $report->generate(); // Generating report
echo $descriptionDecorator->generate(); // Description: Generating report
echo $jsonDecorator->generate(); // "Generating report"
