<?php

namespace wzorce\strukturalne\dekorator\Decorator;

use wzorce\strukturalne\dekorator\Interface\ReportInterface;
use wzorce\strukturalne\dekorator\Model\Report;

abstract class ReportDekorator implements ReportInterface
{
    public function __construct(
        protected Report $report
    ) {
    }

    abstract public function generate(): string;
}