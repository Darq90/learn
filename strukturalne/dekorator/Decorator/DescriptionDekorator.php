<?php

namespace wzorce\strukturalne\dekorator\Decorator;

class DescriptionDekorator  extends ReportDekorator
{
    public function generate(): string
    {
        return 'Description: ' . $this->report->generate();
    }
}