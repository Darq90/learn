<?php

namespace wzorce\strukturalne\dekorator\Decorator;

class JsonDecorator extends ReportDekorator
{
    /**
     * @throws \JsonException
     */
    public function generate(): string
    {
        return json_encode($this->report->generate(), JSON_THROW_ON_ERROR);
    }
}
