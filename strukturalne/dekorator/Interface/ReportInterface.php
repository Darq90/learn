<?php

namespace wzorce\strukturalne\dekorator\Interface;

interface ReportInterface
{
    public function generate(): string;
}