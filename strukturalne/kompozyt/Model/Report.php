<?php

namespace wzorce\strukturalne\kompozyt\Model;

use wzorce\strukturalne\kompozyt\Interface\ReportGenerator;
use wzorce\strukturalne\kompozyt\Interface\ReportSection;

class Report implements ReportSection, ReportGenerator
{
    /** @var ReportSection[] */
    protected array $sections = [];

    public function add(ReportSection $section): self
    {
        $this->sections[] = $section;
        return $this;
    }

    public function generate(): string
    {
        $content = "Generating report:\n";
        foreach ($this->sections as $section) {
            $content .= $section->generate();
        }
        return $content;
    }
}
