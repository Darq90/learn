<?php

namespace wzorce\strukturalne\kompozyt\Model;

use wzorce\strukturalne\kompozyt\Interface\ReportSection;

class ContentSection implements ReportSection
{
    public function generate(): string
    {
        return "Content section\n";
    }
}