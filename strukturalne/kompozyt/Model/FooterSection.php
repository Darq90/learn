<?php

namespace wzorce\strukturalne\kompozyt\Model;

use wzorce\strukturalne\kompozyt\Interface\ReportSection;

class FooterSection implements ReportSection
{
    public function generate(): string
    {
        return "Footer section\n";
    }
}