<?php

namespace wzorce\strukturalne\kompozyt\Model;

use wzorce\strukturalne\kompozyt\Interface\ReportSection;

class TitleSection implements ReportSection
{
    public function generate(): string
    {
        return "Title section\n";
    }
}