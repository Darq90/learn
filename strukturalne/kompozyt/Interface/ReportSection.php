<?php

namespace wzorce\strukturalne\kompozyt\Interface;

interface ReportSection
{
    public function generate(): string;
}