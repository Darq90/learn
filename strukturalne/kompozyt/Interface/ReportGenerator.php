<?php

namespace wzorce\strukturalne\kompozyt\Interface;

interface ReportGenerator
{
    public function add(ReportSection $section): self;
}