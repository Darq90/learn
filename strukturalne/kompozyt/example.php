<?php

use wzorce\strukturalne\kompozyt\Model\ContentSection;
use wzorce\strukturalne\kompozyt\Model\FooterSection;
use wzorce\strukturalne\kompozyt\Model\Report;
use wzorce\strukturalne\kompozyt\Model\TitleSection;

$report = (new Report())
    ->add(new TitleSection())
    ->add(new ContentSection())
    ->add(new FooterSection());

echo $report->generate();
// Generating report:
// Title section
// Content section
// Footer section