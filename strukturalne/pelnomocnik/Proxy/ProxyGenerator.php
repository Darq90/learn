<?php

namespace wzorce\strukturalne\pelnomocnik\Proxy;

use wzorce\strukturalne\pelnomocnik\Interface\ReportGenerator;

class ProxyGenerator implements ReportGenerator
{
    private \wzorce\strukturalne\pelnomocnik\Generator\ReportGenerator|null $generator = null;

    public function generate(): string
    {
        if ($this->generator === null) {
            $this->generator = new \wzorce\strukturalne\pelnomocnik\Generator\ReportGenerator();
        }

        $this->generator->generate();
    }
}
