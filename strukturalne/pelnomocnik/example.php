<?php

use wzorce\strukturalne\pelnomocnik\Proxy\ProxyGenerator;

$reportGenerator = new ProxyGenerator();

$reportGenerator->generate(); // Generating a report...
