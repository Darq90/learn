<?php

namespace wzorce\strukturalne\pelnomocnik\Generator;

class ReportGenerator implements \wzorce\strukturalne\pelnomocnik\Interface\ReportGenerator
{

    public function generate(): string
    {
        echo "Generating a report...\n";
    }
}
