<?php

namespace wzorce\strukturalne\pelnomocnik\Interface;

interface ReportGenerator
{
    public function generate(): string;
}
