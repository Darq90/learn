<?php

namespace wzorce\strukturalne\pylek\Interface;

interface ReportElementInterface
{
    public function render(): string;
}
