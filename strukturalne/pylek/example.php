<?php

use wzorce\strukturalne\pylek\Factory\ReportElementFactory;

$elementFactory = new ReportElementFactory();

$backgroundRed = $elementFactory->getBackground("red");
$backgroundBlue = $elementFactory->getBackground("blue");
$logoA = $elementFactory->getLogo("logoA.png");
$logoB = $elementFactory->getLogo("logoB.png");

echo $backgroundRed->render(); // Background color: red
echo $backgroundBlue->render(); // Background color: blue
echo $logoA->render(); // Logo image: logoA.png
echo $logoB->render(); // Logo image: logoB.png"
