<?php

namespace wzorce\strukturalne\pylek\Factory;

use wzorce\strukturalne\pylek\Interface\ReportElementInterface;
use wzorce\strukturalne\pylek\Model\Background;
use wzorce\strukturalne\pylek\Model\Logo;

class ReportElementFactory
{
    /** @var ReportElementInterface[] */
    private array $elements = [];

    public function getBackground(string $color): Background
    {
        if (!isset($this->elements[$color])) {
            $this->elements[$color] = new Background($color);
        }

        return $this->elements[$color];
    }

    public function getLogo(string $image): Logo
    {
        if (!isset($this->elements[$image])) {
            $this->elements[$image] = new Logo($image);
        }

        return $this->elements[$image];
    }
}
