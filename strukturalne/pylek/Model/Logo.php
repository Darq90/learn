<?php

namespace wzorce\strukturalne\pylek\Model;

use wzorce\strukturalne\pylek\Interface\ReportElementInterface;

readonly class Logo implements ReportElementInterface
{
    public function __construct(private string $image)
    {
    }

    public function render(): string
    {
        return "Logo image: {$this->image}\n";
    }
}
