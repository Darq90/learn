<?php

namespace wzorce\strukturalne\pylek\Model;

use wzorce\strukturalne\pylek\Interface\ReportElementInterface;

readonly class Background implements ReportElementInterface
{
    public function __construct(private string $color)
    {
    }

    public function render(): string
    {
        return "Background color: {$this->color}\n";
    }
}