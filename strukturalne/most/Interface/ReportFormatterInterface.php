<?php

namespace wzorce\strukturalne\most\Interface;

interface ReportFormatterInterface
{
    public function format(string $content): string;
}
