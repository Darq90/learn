<?php

use wzorce\strukturalne\most\Formatter\CSVReportFormatter;
use wzorce\strukturalne\most\Formatter\PDFReportFormatter;
use wzorce\strukturalne\most\Model\CsvReport;
use wzorce\strukturalne\most\Model\PdfReport;

$pdfFormatter = new PDFReportFormatter();
$pdfReport = new PDFReport($pdfFormatter);
$pdfReport->generate(); // PDF Format: Generating PDF report content...

$csvFormatter = new CSVReportFormatter();
$htmlReport = new CsvReport($csvFormatter);
$htmlReport->generate(); // CSV Format: Generating CSV report content...
