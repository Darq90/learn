<?php

namespace wzorce\strukturalne\most\Formatter;

use wzorce\strukturalne\most\Interface\ReportFormatterInterface;

class CSVReportFormatter implements ReportFormatterInterface
{
    public function format(string $content): string
    {
        return "CSV Format: " . $content;
    }
}
