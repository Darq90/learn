<?php

namespace wzorce\strukturalne\most\Formatter;

use wzorce\strukturalne\most\Interface\ReportFormatterInterface;

class PDFReportFormatter implements ReportFormatterInterface
{
    public function format(string $content): string
    {
        return "PDF Format: " . $content;
    }
}