<?php

namespace wzorce\strukturalne\most\Model;

class CsvReport extends Report
{

    public function generate(): void
    {
        $content = "Generating CSV report content...";
        print $this->formatter->format($content);
    }
}