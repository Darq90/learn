<?php

namespace wzorce\strukturalne\most\Model;

class PdfReport extends Report
{

    public function generate(): void
    {
        $content = "Generating PDF report content...";
        print $this->formatter->format($content);
    }
}