<?php

namespace wzorce\strukturalne\most\Model;

use wzorce\strukturalne\most\Interface\ReportFormatterInterface;

abstract class Report
{
    public function __construct(
        protected ReportFormatterInterface $formatter
    ) {
    }

    abstract public function generate(): void;
}
