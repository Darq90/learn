<?php

namespace wzorce\kreacyjne\prototyp\Model;

use wzorce\kreacyjne\prototyp\Interface\ReportInterface;

class CsvReport extends Report
{
    public function cloneReport(): ReportInterface
    {
        return (new self())
            ->setHeader($this->header)
            ->setContent($this->content)
            ->setFooter($this->footer);
    }
}
