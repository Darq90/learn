<?php

namespace wzorce\kreacyjne\prototyp\Model;

use wzorce\kreacyjne\prototyp\Interface\ReportInterface;

abstract class Report implements ReportInterface
{
    protected string $header;
    protected string $content;
    protected string $footer;

    public function setHeader($header): self
    {
        $this->header = $header;
        return $this;
    }

    public function setContent($content): self
    {
        $this->content = $content;
        return $this;
    }

    public function setFooter($footer): self
    {
        $this->footer = $footer;
        return $this;
    }

    abstract public function cloneReport(): ReportInterface;

    public function print(): void
    {
        echo $this->header . ' ' . $this->content . ' ' . $this->footer;
    }
}
