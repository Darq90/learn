<?php

use wzorce\Helpers\Enums\ReportTypeEnum;
use wzorce\kreacyjne\prototyp\Model\CsvReport;
use wzorce\kreacyjne\prototyp\Model\PdfReport;

$reportPdf = (new PdfReport())
    ->setHeader(ReportTypeEnum::PDF->name . " Header")
    ->setContent(ReportTypeEnum::PDF->name . " Content")
    ->setFooter(ReportTypeEnum::PDF->name . " Footer");

$pdfClone = $reportPdf->cloneReport();
$pdfClone->setContent("Modified PDF Content");

$reportCsv = (new CsvReport())
    ->setHeader(ReportTypeEnum::CSV->name . " Header")
    ->setContent(ReportTypeEnum::CSV->name . " Content")
    ->setFooter(ReportTypeEnum::CSV->name . " Footer");

$csvClone = $reportCsv->cloneReport();
$csvClone->setContent("Modified CSV Content");

$reportPdf->print(); // PdfHeader PdfContent PdfFooter
$pdfClone->print(); // PdfHeader Modified PDF Content PdfFooter
$reportCsv->print(); // CsvHeader CsvContent CsvFooter
$csvClone->print(); // CsvHeader Modified CSV Content CsvFooter
