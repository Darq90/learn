<?php

namespace wzorce\kreacyjne\prototyp\Interface;

interface ReportInterface
{
    public function setHeader(string $header): self;
    public function setContent(string $content): self;
    public function setFooter(string $footer): self;
    public function print(): void;
}
