<?php

use wzorce\Helpers\Enums\ReportTypeEnum;
use wzorce\kreacyjne\fabryka_abstrakcyjna\Factory\ReportFactory;

$reportGenerator = (new ReportFactory())
    ->create(ReportTypeEnum::HTML)
    ->setData('Message 1');

$result = $reportGenerator->generate();

echo $result->getContent();
// <html><body>Message 1</body></html>

echo $result->getExtension();
// html

$reportGenerator = (new ReportFactory())
    ->create(ReportTypeEnum::CSV)
    ->setData('Message 2');

$result = $reportGenerator->generate();

echo $result->getContent();
// Message;2

echo $result->getExtension();
// csv

/*
Aplikacja może wspierać tworzenie 2 różnych typów raportów. HTML oraz CSV.
Każda z danych wejściowych to inna dana