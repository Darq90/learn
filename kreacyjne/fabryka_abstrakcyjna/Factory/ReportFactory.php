<?php

namespace wzorce\kreacyjne\fabryka_abstrakcyjna\Factory;

use wzorce\Helpers\Enums\ReportTypeEnum;
use wzorce\Helpers\Exception\NotImplementedException;
use wzorce\kreacyjne\fabryka_abstrakcyjna\Interface\ReportFactoryInterface;
use wzorce\kreacyjne\fabryka_abstrakcyjna\interface\ReportInterface;
use wzorce\kreacyjne\fabryka_abstrakcyjna\Model\CsvReportGenerator;
use wzorce\kreacyjne\fabryka_abstrakcyjna\Model\HtmlReportGenerator;

class ReportFactory implements ReportFactoryInterface
{
    /**
     * @throws \Exception
     */
    public function create(ReportTypeEnum $reportType): ReportInterface
    {
        return match ($reportType) {
            ReportTypeEnum::CSV => new CsvReportGenerator(),
            ReportTypeEnum::HTML => new HtmlReportGenerator(),
            ReportTypeEnum::TXT, ReportTypeEnum::PDF => throw new NotImplementedException()
        };
    }
}
