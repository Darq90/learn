<?php

namespace wzorce\kreacyjne\fabryka_abstrakcyjna\ReportGenerator;

use wzorce\kreacyjne\fabryka_abstrakcyjna\Model\ReportResult;

class CsvReportGenerator extends Report
{
    protected const EXTENSION = 'csv';

    public function generate(): ReportResult
    {
        return new ReportResult(str_replace(' ', ';', $this->data), static::EXTENSION);
    }
}
