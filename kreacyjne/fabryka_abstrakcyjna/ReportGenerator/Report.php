<?php

namespace wzorce\kreacyjne\fabryka_abstrakcyjna\ReportGenerator;

use wzorce\kreacyjne\fabryka_abstrakcyjna\Interface\ReportInterface;
use wzorce\kreacyjne\fabryka_abstrakcyjna\Model\ReportResult;

abstract class Report implements ReportInterface
{
    protected CONST EXTENSION = '';
    protected string $data;

    public function setData(string $data): self
    {
        $this->data = $data;
        return $this;
    }

    abstract public function generate(): ReportResult;
}
