<?php

namespace wzorce\kreacyjne\fabryka_abstrakcyjna\ReportGenerator;

use wzorce\kreacyjne\fabryka_abstrakcyjna\Model\ReportResult;

class HtmlReportGenerator extends Report
{
    protected const EXTENSION = 'html';

    public function generate(): ReportResult
    {
        return new ReportResult("<html><body>{$this->data}</body></html>", static::EXTENSION);
    }
}
