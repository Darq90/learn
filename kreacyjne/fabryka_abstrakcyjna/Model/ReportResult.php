<?php

namespace wzorce\kreacyjne\fabryka_abstrakcyjna\Model;

readonly class ReportResult
{
    public function __construct(
        private string $content,
        private string $extension
    )
    {
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }
}