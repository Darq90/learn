<?php

namespace wzorce\kreacyjne\fabryka_abstrakcyjna\Interface;

use wzorce\Helpers\Enums\ReportTypeEnum;

interface ReportFactoryInterface
{
    public function create(ReportTypeEnum $reportType): ReportInterface;
}
