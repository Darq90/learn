<?php

namespace wzorce\kreacyjne\fabryka_abstrakcyjna\Interface;

use wzorce\kreacyjne\fabryka_abstrakcyjna\Model\ReportResult;

interface ReportInterface
{
    public function setData(string $data): self;
    public function generate(): ReportResult;
}
