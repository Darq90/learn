<?php

use wzorce\kreacyjne\fanryka_abstrakcyjna\Factory\CsvReportFactory;
use wzorce\kreacyjne\fanryka_abstrakcyjna\Factory\PdfReportFactory;

$reportGenerator = (new PdfReportFactory())->create();
echo $reportGenerator->generate(); // 'PdfReport'

$reportGenerator = (new CsvReportFactory())->create();
echo $reportGenerator->generate(); // 'CsvReport'