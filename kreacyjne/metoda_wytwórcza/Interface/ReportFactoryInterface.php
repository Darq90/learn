<?php

namespace wzorce\kreacyjne\metoda_wytwórcza\Interface;

interface ReportFactoryInterface
{
    public function create(): ReportInterface;
}