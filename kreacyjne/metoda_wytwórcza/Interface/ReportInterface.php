<?php

namespace wzorce\kreacyjne\metoda_wytwórcza\Interface;

interface ReportInterface
{
    public function generate(): string;
}