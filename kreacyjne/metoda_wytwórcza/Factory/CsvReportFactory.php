<?php

namespace wzorce\kreacyjne\metoda_wytwórcza\Factory;

use wzorce\kreacyjne\metoda_wytwórcza\Interface\ReportFactoryInterface;
use wzorce\kreacyjne\metoda_wytwórcza\interface\ReportInterface;
use wzorce\kreacyjne\metoda_wytwórcza\Model\CsvReport;

class CsvReportFactory implements ReportFactoryInterface
{
    public function create(): ReportInterface
    {
        return new CsvReport();
    }
}