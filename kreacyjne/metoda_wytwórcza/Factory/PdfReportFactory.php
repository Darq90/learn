<?php

namespace wzorce\kreacyjne\metoda_wytwórcza\Factory;

use wzorce\kreacyjne\metoda_wytwórcza\Interface\ReportFactoryInterface;
use wzorce\kreacyjne\metoda_wytwórcza\Interface\ReportInterface;
use wzorce\kreacyjne\metoda_wytwórcza\Model\PdfReport;

class PdfReportFactory implements ReportFactoryInterface
{
    public function create(): ReportInterface
    {
        return new PdfReport();
    }
}