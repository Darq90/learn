<?php

namespace wzorce\kreacyjne\metoda_wytwórcza\Model;

use wzorce\Helpers\Enums\ReportTypeEnum;
use wzorce\kreacyjne\metoda_wytwórcza\Interface\ReportInterface;

class CsvReport implements ReportInterface
{
    public function generate(): string
    {
        return ReportTypeEnum::CSV->name . 'Report';
    }
}