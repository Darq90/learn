<?php

namespace wzorce\kreacyjne\budowniczy\Builder;

use wzorce\kreacyjne\budowniczy\Interface\ReportBuilderInterface;
use wzorce\kreacyjne\budowniczy\Model\Report;

class ReportBuilder implements ReportBuilderInterface
{
    private Report $report;

    public function __construct()
    {
        $this->report = new Report();
    }

    public function setHeader(string $header): self
    {
        $this->report->addPart("header: {$header}");
    }

    public function setContent(string $content): self
    {
        $this->report->addPart("content: {$content}");
    }

    public function setFooter(string $footer): self
    {
        $this->report->addPart("footer: {$footer}");
    }

    public function print(): void
    {
        $this->report->print();
    }
}
