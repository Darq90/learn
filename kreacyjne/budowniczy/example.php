<?php


use wzorce\Helpers\Enums\ReportTypeEnum;
use wzorce\kreacyjne\budowniczy\Builder\ReportBuilder;

$reportGenerator = (new ReportBuilder())
    ->setHeader(ReportTypeEnum::PDF->name . '__header')
    ->setContent(ReportTypeEnum::PDF->name . '__content')
    ->setFooter(ReportTypeEnum::PDF->name . '__footer');

$reportGenerator->print();
// header: Pdf__header
// content: Pdf__content
// footer: Pdf__footer


$reportGenerator = (new ReportBuilder())
    ->setHeader(ReportTypeEnum::CSV->name . '__header')
    ->setContent(ReportTypeEnum::CSV->name . '__content')
    ->setFooter(ReportTypeEnum::CSV->name . '__footer');

$reportGenerator->print();
// header: Csv__header
// content: Csv__content
// footer: Csv__footer
