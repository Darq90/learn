<?php

namespace wzorce\kreacyjne\budowniczy\Interface;

interface ReportInterface
{
    public function addPart(string $part): self;
    public function print(): self;
}
