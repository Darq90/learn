<?php

namespace wzorce\kreacyjne\budowniczy\Interface;

interface ReportBuilderInterface
{
    public function setHeader(string $header): self;
    public function setContent(string $content): self;
    public function setFooter(string $footer): self;
    public function print(): void;
}
