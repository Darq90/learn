<?php

namespace wzorce\kreacyjne\budowniczy\Model;

use wzorce\kreacyjne\budowniczy\Interface\ReportInterface;

class Report implements ReportInterface
{
    /**
     * @var string[]
     */
    private array $parts = [];

    public function addPart(string $part): self
    {
        $this->parts[] = $part;
        return $this;
    }

    public function print(): self
    {
        foreach ($this->parts as $part) {
            echo $part . PHP_EOL;
        }

        return $this;
    }
}
