<?php

namespace wzorce\kreacyjne\singleton\Interface;

use wzorce\Helpers\Enums\ReportTypeEnum;

interface ReportInterface
{
    public static function getInstance(): self;

    public function generateReport(ReportTypeEnum $reportType): void;
}
