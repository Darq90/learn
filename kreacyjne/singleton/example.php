<?php

use wzorce\Helpers\Enums\ReportTypeEnum;
use wzorce\kreacyjne\singleton\Model\CsvReport;
use wzorce\kreacyjne\singleton\Model\PdfReport;

$pdfReport = PdfReport::getInstance();
$pdfReport->generateReport(ReportTypeEnum::PDF); // Generating Pdf report...

$htmlReport = CsvReport::getInstance();
$htmlReport->generateReport(ReportTypeEnum::CSV); // Generating Csv report...
