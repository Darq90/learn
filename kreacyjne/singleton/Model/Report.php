<?php

namespace wzorce\kreacyjne\singleton\Model;

use wzorce\Helpers\Enums\ReportTypeEnum;
use wzorce\kreacyjne\singleton\Interface\ReportInterface;

class Report implements ReportInterface
{
    private static array $instances = [];

    protected function __construct() {
    }

    public static function getInstance(): self
    {
        $className = static::class;

        if (!isset(self::$instances[$className])) {
            self::$instances[$className] = new static();
        }

        return self::$instances[$className];
    }

    public function generateReport(ReportTypeEnum $reportType): void
    {
        print "Generating {$reportType->name} report...";
    }
}
