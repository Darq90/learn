<?php

namespace wzorce\behawioralne\mediator\Interface;

interface ReportMediatorInterface
{
    public function generatePDF();
    public function generateHTML();
    public function generateCSV();
}
