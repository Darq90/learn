<?php

use wzorce\behawioralne\mediator\Generator\ReportGenerator;
use wzorce\behawioralne\mediator\Mediator\ReportMediator;

$mediator = new ReportMediator();
$generator = new ReportGenerator($mediator);

$generator->generatePDFReport();
$generator->generateHTMLReport();
$generator->generateCSVReport();

// Generating PDF report...
// PDF report generated.
// Generating HTML report...
// HTML report generated.
// Generating CSV report...
// CSV report generated.