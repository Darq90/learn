<?php

namespace wzorce\behawioralne\mediator\Mediator;

use wzorce\behawioralne\mediator\Interface\ReportMediatorInterface;

class ReportMediator implements ReportMediatorInterface
{
    public function generatePDF(): string
    {
        echo "PDF report generated.\n";
    }

    public function generateHTML(): string
    {
        echo "HTML report generated.\n";
    }

    public function generateCSV():string
    {
        echo "CSV report generated.\n";
    }
}
