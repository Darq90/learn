<?php

namespace wzorce\behawioralne\mediator\Generator;

use wzorce\behawioralne\mediator\Mediator\ReportMediator;

class ReportGenerator
{
    private ReportMediator $mediator;

    public function __construct(ReportMediator $mediator) {
        $this->mediator = $mediator;
    }

    public function generatePDFReport(): string
    {
        echo "Generating PDF report...\n";
        $this->mediator->generatePDF();
    }

    public function generateHTMLReport(): string
    {
        echo "Generating HTML report...\n";
        $this->mediator->generateHTML();
    }

    public function generateCSVReport(): string
    {
        echo "Generating CSV report...\n";
        $this->mediator->generateCSV();
    }
}
