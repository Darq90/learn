<?php

use wzorce\behawioralne\odwiedzajacy\Reports\CsvReport;
use wzorce\behawioralne\odwiedzajacy\Reports\PdfReport;
use wzorce\behawioralne\odwiedzajacy\Visitors\Visitor;

$pdfReport = new PDFReport();
$csvReport = new CSVReport();

$visitor = new Visitor();

$pdfReport->accept($visitor);
// PDF report info: ...

$csvReport->accept($visitor);
// CSV report info: ...