<?php

namespace wzorce\behawioralne\odwiedzajacy\Visitors;

use wzorce\behawioralne\odwiedzajacy\Interface\VisitorInterface;
use wzorce\behawioralne\odwiedzajacy\Reports\CsvReport;
use wzorce\behawioralne\odwiedzajacy\Reports\PdfReport;

class Visitor implements VisitorInterface
{
    public function visitPDFReport(PDFReport $report): void
    {
        echo "PDF report info: ...\n";
    }

    public function visitCSVReport(CSVReport $report): void
    {
        echo "CSV report info: ...\n";
    }
}
