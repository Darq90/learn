<?php

namespace wzorce\behawioralne\odwiedzajacy\Interface;

use wzorce\behawioralne\odwiedzajacy\Reports\CsvReport;
use wzorce\behawioralne\odwiedzajacy\Reports\PdfReport;

interface VisitorInterface
{
    public function visitPDFReport(PDFReport $report);
    public function visitCSVReport(CSVReport $report);
}