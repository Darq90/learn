<?php

namespace wzorce\behawioralne\odwiedzajacy\Interface;

interface ReportInterface
{
    public function accept(VisitorInterface $visitor);
}