<?php

namespace wzorce\behawioralne\odwiedzajacy\Reports;

use wzorce\behawioralne\odwiedzajacy\Interface\ReportInterface;
use wzorce\behawioralne\odwiedzajacy\Interface\VisitorInterface;

class PdfReport implements ReportInterface
{
    public function accept(VisitorInterface $visitor): void
    {
        $visitor->visitPDFReport($this);
    }
}
