<?php

namespace wzorce\behawioralne\odwiedzajacy\Reports;

use wzorce\behawioralne\odwiedzajacy\Interface\VisitorInterface;

class CsvReport
{
    public function accept(VisitorInterface $visitor): void
    {
        $visitor->visitCSVReport($this);
    }
}
