<?php

namespace wzorce\behawioralne\strategia\Interface;

interface ReportStrategyInterface
{
    public function generate(): void;
}
