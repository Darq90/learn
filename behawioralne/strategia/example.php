<?php

use wzorce\behawioralne\strategia\Report\ReportContext;
use wzorce\behawioralne\strategia\Strategy\CsvReportStrategy;
use wzorce\behawioralne\strategia\Strategy\PdfReportStrategy;

$context = new ReportContext();

$pdfStrategy = new PDFReportStrategy();
$csvStrategy = new CSVReportStrategy();

$context->setStrategy($pdfStrategy);
$context->generateReport(); // Generating PDF report...

$context->setStrategy($csvStrategy);
$context->generateReport(); // Generating CSV report...
