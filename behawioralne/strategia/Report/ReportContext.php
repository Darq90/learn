<?php

namespace wzorce\behawioralne\strategia\Report;

use wzorce\behawioralne\strategia\Interface\ReportStrategyInterface;

class ReportContext
{
    private ReportStrategyInterface $strategy;

    public function setStrategy(ReportStrategyInterface $strategy): self
    {
        $this->strategy = $strategy;
    }

    public function generateReport(): void
    {
        $this->strategy->generate();
    }
}
