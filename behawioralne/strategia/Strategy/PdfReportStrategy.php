<?php

namespace wzorce\behawioralne\strategia\Strategy;

use wzorce\behawioralne\strategia\Interface\ReportStrategyInterface;

class PdfReportStrategy implements ReportStrategyInterface
{

    public function generate(): void
    {
        echo "Generating PDF report...\n";
    }
}