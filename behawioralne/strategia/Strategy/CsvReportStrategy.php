<?php

namespace wzorce\behawioralne\strategia\Strategy;

use wzorce\behawioralne\strategia\Interface\ReportStrategyInterface;

class CsvReportStrategy implements ReportStrategyInterface
{

    public function generate(): void
    {
        echo "Generating CSV report...\n";
    }
}