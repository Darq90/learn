<?php

namespace wzorce\behawioralne\metoda_szablonowa\Report;

use wzorce\behawioralne\metoda_szablonowa\Template\ReportTemplate;

class PDFReport extends ReportTemplate
{
    protected function generateHeader(): string
    {
        echo "Generating PDF report header...\n";
    }

    protected function generateBody(): string
    {
        echo "Generating PDF report body...\n";
    }

    protected function generateFooter(): string
    {
        echo "Generating PDF report footer...\n";
    }
}
