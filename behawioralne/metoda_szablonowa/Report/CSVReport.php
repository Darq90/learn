<?php

namespace wzorce\behawioralne\metoda_szablonowa\Report;

use wzorce\behawioralne\metoda_szablonowa\Template\ReportTemplate;

class CSVReport extends ReportTemplate
{
    protected function generateHeader(): string
    {
        echo "Generating CSV report header...\n";
    }

    protected function generateBody(): string
    {
        echo "Generating CSV report body...\n";
    }

    protected function generateFooter(): string
    {
        echo "Generating CSV report footer...\n";
    }
}
