<?php

namespace wzorce\behawioralne\metoda_szablonowa\Template;

use wzorce\behawioralne\metoda_szablonowa\Interface\ReportInterface;

abstract class ReportTemplate implements ReportInterface
{
    final public function generateReport(): void
    {
        $this->generateHeader();
        $this->generateBody();
        $this->generateFooter();
    }

    abstract protected function generateHeader();
    abstract protected function generateBody();
    abstract protected function generateFooter();
}
