<?php

use wzorce\behawioralne\metoda_szablonowa\Report\CSVReport;
use wzorce\behawioralne\metoda_szablonowa\Report\PDFReport;

(new PDFReport())->generateReport();
// Generating PDF report header...
// Generating PDF report body...
// Generating PDF report body...

(new CSVReport())->generateReport();
// Generating CSV report header...
// Generating CSV report body...
// Generating CSV report body...
