<?php

namespace wzorce\behawioralne\metoda_szablonowa\Interface;

interface ReportInterface
{
    public function generateReport(): void;
}
