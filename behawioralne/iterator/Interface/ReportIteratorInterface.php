<?php

namespace wzorce\behawioralne\iterator\Interface;

interface ReportIteratorInterface
{
    public function hasNext(): bool;
    public function next(): self|null;
}

