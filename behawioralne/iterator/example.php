<?php

use wzorce\behawioralne\iterator\Collection\ReportCollection;

$reportCollection = new ReportCollection();
$reportCollection->addReport("PDF Report");
$reportCollection->addReport("TXT Report");
$reportCollection->addReport("CSV Report");

while ($reportCollection->hasNext()) {
    $report = $reportCollection->next();
    echo "Report: {$report}\n";
}

// Report: PDF Report
// Report: TXT Report
// Report: CSV Report