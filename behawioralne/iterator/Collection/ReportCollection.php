<?php

namespace wzorce\behawioralne\iterator\Collection;

use wzorce\behawioralne\iterator\Interface\ReportIteratorInterface;

class ReportCollection implements ReportIteratorInterface
{
    /** @var ReportIteratorInterface[]  */
    private array $reports = [];
    private int $currentIndex = 0;

    public function addReport($report): self
    {
        $this->reports[] = $report;
    }

    public function hasNext(): bool
    {
        return $this->currentIndex < count($this->reports);
    }

    public function next(): ReportIteratorInterface|null
    {
        if ($this->hasNext()) {
            $report = $this->reports[$this->currentIndex];
            $this->currentIndex++;
            return $report;
        }
        return null;
    }
}
