<?php

use wzorce\behawioralne\lancuch_zoobowiazan\Handlers\CsvReportHandler;
use wzorce\behawioralne\lancuch_zoobowiazan\Handlers\PdfReportHandler;
use wzorce\Helpers\Enums\ReportTypeEnum;

$pdfHandler = new PdfReportHandler();
$csvHandler = new CsvReportHandler();

$pdfHandler->setNext($csvHandler);

echo $pdfHandler->handleRequest(ReportTypeEnum::PDF);
echo $pdfHandler->handleRequest(ReportTypeEnum::CSV);
echo $pdfHandler->handleRequest(ReportTypeEnum::TXT);
