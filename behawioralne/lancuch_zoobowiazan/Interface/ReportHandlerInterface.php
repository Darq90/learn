<?php

namespace wzorce\behawioralne\lancuch_zoobowiazan\Interface;

use wzorce\Helpers\Enums\ReportTypeEnum;

interface ReportHandlerInterface
{
    public function setNext(self $handler): self;
    public function handleRequest(ReportTypeEnum $reportType): string;
}
