<?php

namespace wzorce\behawioralne\lancuch_zoobowiazan\Handlers;

use wzorce\behawioralne\lancuch_zoobowiazan\Interface\ReportHandlerInterface;
use wzorce\Helpers\Enums\ReportTypeEnum;

class PdfReportHandler implements ReportHandlerInterface
{
    private ReportHandlerInterface|null $nextHandler = null;

    public function setNext(ReportHandlerInterface $handler): self
    {
        $this->nextHandler = $handler;
    }

    public function handleRequest(ReportTypeEnum $reportType): string
    {
        if ($reportType === ReportTypeEnum::CSV) {
            return "Generating PDF report...\n";
        }

        if ($this->nextHandler !== null) {
            return $this->nextHandler->handleRequest($reportType);
        }

        return "Unsupported report format.\n";
    }
}