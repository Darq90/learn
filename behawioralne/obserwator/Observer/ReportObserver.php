<?php

namespace wzorce\behawioralne\obserwator\Observer;

class ReportObserver
{
    private string $name;

    public function __construct(string $name) {
        $this->name = $name;
    }

    public function update(string $reportContent): void
    {
        echo "$this->name got notified. New content: {$reportContent}";
    }
}
