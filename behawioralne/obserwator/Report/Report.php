<?php

namespace wzorce\behawioralne\obserwator\Report;

use wzorce\behawioralne\obserwator\Observer\ReportObserver;

class Report
{
    /**
     * @var ReportObserver[]
     */
    private array $observers = [];
    private string $reportContent;

    public function attach(ReportObserver $observer): void
    {
        $this->observers[] = $observer;
    }

    public function detach(ReportObserver $observer): void
    {
        $key = array_search($observer, $this->observers, true);
        if ($key !== false) {
            unset($this->observers[$key]);
        }
    }

    public function setContent(string $content): void
    {
        $this->reportContent = $content;
        $this->notifyObservers();
    }

    public function getContent(): string
    {
        return $this->reportContent;
    }

    private function notifyObservers(): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this->reportContent);
        }
    }
}
