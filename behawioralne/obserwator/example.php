<?php

use wzorce\behawioralne\obserwator\Observer\ReportObserver;
use wzorce\behawioralne\obserwator\Report\Report;

$reportSubject = new Report();

$observerA = new ReportObserver("Observer A");
$observerB = new ReportObserver("Observer B");

$reportSubject->attach($observerA);
$reportSubject->attach($observerB);

$reportSubject->setContent("New report content 1");
$reportSubject->setContent("New report content 2");

$reportSubject->detach($observerA);

$reportSubject->setContent("New report content 3");

// Observer A got notified. New content: New report content 1
// Observer B got notified. New content: New report content 2
// Observer B got notified. New content: New report content 3
