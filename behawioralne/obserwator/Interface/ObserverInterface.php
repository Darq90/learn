<?php

namespace wzorce\behawioralne\obserwator\Interface;

interface ObserverInterface
{
    public function update(string $reportContent): self;
}
