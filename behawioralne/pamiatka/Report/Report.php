<?php

namespace wzorce\behawioralne\pamiatka\Report;

use wzorce\behawioralne\pamiatka\Memento\ReportMemento;

class Report
{
    private string $content;

    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function createMemento(): ReportMemento
    {
        return new ReportMemento($this->content);
    }

    public function restoreMemento(ReportMemento $memento): self
    {
        $this->content = $memento->getState();
        return $this;
    }
}
