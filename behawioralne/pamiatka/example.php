<?php

use wzorce\behawioralne\pamiatka\Report\Report;

$report = new Report();
$report->setContent("Initial content");


$memento = $report->createMemento();
$report->setContent("Updated content");
$report->restoreMemento($memento);

echo "Current content: " . $report->getContent();
// Initial content
