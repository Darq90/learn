<?php

namespace wzorce\behawioralne\stan\Interface;

interface ReportStateInterface
{
    public function generate(): void;
}