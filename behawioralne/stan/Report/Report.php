<?php

namespace wzorce\behawioralne\State;

use wzorce\behawioralne\stan\Interface\ReportStateInterface;
use wzorce\behawioralne\stan\State\ReportCreatingState;

class Report
{
    private ReportStateInterface $state;

    public function __construct() {
        $this->setState(new ReportCreatingState());
    }

    public function setState(ReportStateInterface $state): void
    {
        $this->state = $state;
    }

    public function generateReport(): void
    {
        $this->state->generate();
    }
}
