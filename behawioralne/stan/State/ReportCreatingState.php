<?php

namespace wzorce\behawioralne\stan\State;

use wzorce\behawioralne\stan\Interface\ReportStateInterface;

class ReportCreatingState implements ReportStateInterface
{
    public function generate(): void
    {
        echo "Report is being created...\n";
    }
}
