<?php

namespace wzorce\behawioralne\stan\State;

use wzorce\behawioralne\stan\Interface\ReportStateInterface;

class ReportReadyState implements ReportStateInterface
{
    public function generate(): void
    {
        echo "Report is ready to generate...\n";
    }
}
