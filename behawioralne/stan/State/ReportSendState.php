<?php

namespace wzorce\behawioralne\stan\State;

use wzorce\behawioralne\stan\Interface\ReportStateInterface;

class ReportSendState implements ReportStateInterface
{
    public function generate(): void
    {
        echo "Report has been sent...\n";
    }
}
