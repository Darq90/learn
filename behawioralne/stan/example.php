<?php

use wzorce\behawioralne\stan\State\ReportReadyState;
use wzorce\behawioralne\stan\State\ReportSendState;

$report = new wzorce\behawioralne\State\Report();

$report->generateReport(); // Report is being created...
$report->setState(new ReportReadyState());
$report->generateReport(); // Report is ready to generate...
$report->setState(new ReportSendState());
$report->generateReport(); // Report has been sent
