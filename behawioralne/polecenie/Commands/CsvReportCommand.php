<?php

namespace wzorce\behawioralne\polecenie\Commands;

use wzorce\behawioralne\polecenie\Interface\ReportCommandInterface;

class CsvReportCommand implements ReportCommandInterface
{
    public function execute(): string
    {
        return "Generating CSV report...\n";
    }
}