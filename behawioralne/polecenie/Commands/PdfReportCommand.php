<?php

namespace wzorce\behawioralne\polecenie\Commands;

use wzorce\behawioralne\polecenie\Interface\ReportCommandInterface;

class PdfReportCommand implements ReportCommandInterface
{
    public function execute(): string
    {
        return "Generating PDF report...\n";
    }
}