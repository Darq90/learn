<?php

use wzorce\behawioralne\polecenie\Commands\CsvReportCommand;
use wzorce\behawioralne\polecenie\Commands\PdfReportCommand;
use wzorce\behawioralne\polecenie\Invokers\ReportGeneratorInvoker;

$invoker = new ReportGeneratorInvoker();

$pdfCommand = new PdfReportCommand();
$csvCommand = new CsvReportCommand();

$invoker->addCommand($pdfCommand);
$invoker->addCommand($csvCommand);

$output = $invoker->runCommands();
echo $output;
// Generating PDF report...
// Generating CSV report...
