<?php

namespace wzorce\behawioralne\polecenie\Interface;

interface ReportCommandInterface
{
    public function execute(): string;
}
