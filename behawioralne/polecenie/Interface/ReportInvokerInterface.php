<?php

namespace wzorce\behawioralne\polecenie\Interface;

interface ReportInvokerInterface
{
    public function addCommand(ReportCommandInterface $command): self;
    public function runCommands(): string;
}
