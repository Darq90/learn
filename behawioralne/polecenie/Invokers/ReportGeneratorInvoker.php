<?php

namespace wzorce\behawioralne\polecenie\Invokers;

use wzorce\behawioralne\polecenie\Interface\ReportCommandInterface;
use wzorce\behawioralne\polecenie\Interface\ReportInvokerInterface;

class ReportGeneratorInvoker implements ReportInvokerInterface
{
    /** @var ReportCommandInterface[]  */
    private array $commands = [];

    public function addCommand(ReportCommandInterface $command): self
    {
        $this->commands[] = $command;
        return $this;
    }

    public function runCommands(): string
    {
        $output = '';

        foreach ($this->commands as $command) {
            $output .= $command->execute();
        }

        return $output;
    }
}
